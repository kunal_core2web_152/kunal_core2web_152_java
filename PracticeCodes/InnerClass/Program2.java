class Outer{

	int x=10;
	void fun(){
		System.out.println("In fun-Outer");
	}

	class Inner{
		Inner(){
			System.out.println("Inner Constructor");
		}
	}
}
class Client{
	public static void main(String[] args){
		Outer obj1=new Outer();
		Outer.Inner obj=new Outer().new Inner();
		

	}
}
