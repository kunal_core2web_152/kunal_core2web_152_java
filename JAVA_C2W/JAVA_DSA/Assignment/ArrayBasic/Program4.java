import java.util.*;

class LargestElement{
	public static void main(String[] args){

		int arr[]=new int[]{1,8,7,56,90};

		Arrays.sort(arr);

		int largeElement=arr[arr.length-1];

		System.out.println("Largest Element is : " + largeElement);
	}
}


