class Parent{
	void fun(){
		System.out.println("In fun-parent");
	}

	void run(){
		System.out.println("In run-parent");
	}
}

class Child extends Parent{
	void fun(){
		System.out.println("In fun-child");
	}

	void run(){
		System.out.println("In run-child");
	}

	void gun(){
		System.out.println("In gun-child");
	}
}

class Client{
	public static void main(String[] args){

		Parent p=new Child();
		p.fun();
		p.run();
		p.gun();	//error
	}
}
