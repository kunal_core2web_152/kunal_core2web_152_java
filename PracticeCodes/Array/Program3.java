import java.util.*;

class Program3{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the array size: ");
		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter the array elements: ");
		

		for(int i=0; i<arr.length; i++){
			 arr[i]=sc.nextInt();
			}
		
		int sum=0;
		
		for(int i=0; i<arr.length; i++){
			if(i%2 == 0){
				sum=sum+i;
				}
			}
			System.out.println("Sum of odd elements " +sum);
	}
}	
