class Outer{
	static int x=10;
	int y=20;

	static void run(){
		System.out.println("In fun");
	}

	void gun(){
		System.out.println("In gun");
	}

	static Inner class{
		Inner(){
			System.out.println(x);
			System.out.println(y);
			run();
			gun();
		}
	}
	public static void main(String[] args){
		Outer obj=new Outer();
		Inner obj1=obj.new Inner();
	}
}
