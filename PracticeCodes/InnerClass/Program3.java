class Outer{
	int x=10;
	static int y=20;

	Outer(){
		System.out.println("Outer-COnst");
	}

	class Inner{
		Inner(){
			static int x=20;

			static void run(){
				System.out.println("In run");
			}
			System.out.println("Inner Const");
			System.out.println(x);
			System.out.println(y);
		}
	}
	public static void main(String[] args){

		//Outer obj1=new Outer();
		Inner obj2=new Outer().new Inner();
	}
}
