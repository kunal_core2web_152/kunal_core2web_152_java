class Demo{
	int x=10;

	Demo(){
		this(10);
		System.out.println("In No-args");
	}

	Demo(int x){
		this();
		System.out.println("In Para");
	}

	public static void main(String[] args){

		Demo obj1=new Demo();
		Demo obj2=new Demo(10);
	}
}
