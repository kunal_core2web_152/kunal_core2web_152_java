class Parent{
	char fun(){
		System.out.println("In Parent-fun");
		return 'A';
	}
}

class Child extends Parent{
	char fun(){
		System.out.println("In child-fun");
		return 10;
	}
}

class Client{
	public static void main(String[] args){
		Child obj=new Child();
		obj.fun();
	}
}
