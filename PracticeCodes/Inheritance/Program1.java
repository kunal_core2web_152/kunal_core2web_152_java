class Parent{
	int x=10;
	void fun(){
		System.out.println("In fun-parent");
	}
}
class Child extends Parent{
	int x=20;
	void fun(){
		System.out.println("In fun-child");
		System.out.println(super.x);
	}
}
class Client{
	public static void main(String[] args){
		Parent obj=new Child();
		obj.fun();
	}
}
