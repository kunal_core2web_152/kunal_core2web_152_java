import java.io.*;

class ExceptionDemo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int x=0;
		int y=0;
		System.out.println("Enter value for x and y");

		try{
			x=Integer.parseInt(br.readLine());
			y=Integer.parseInt(br.readLine());
			System.out.println(x/y);
		}catch(IOException ie){
			System.out.println("Connection break");
		}catch(NumberFormatException nfe){
			System.out.println("Wrong Input");
		}catch(ArithmeticException ae){
			System.out.println("Please Enter y value again");
		       	y=Integer.parseInt(br.readLine());
			System.out.println(x/y);
		}finally{
			System.out.println("Cleanup-Code");
		}	
		System.out.println("End main");
	}
}

