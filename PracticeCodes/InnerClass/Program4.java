class Outer{
	class Inner1{
		Inner1(){
			System.out.println("In Inner1");
		}

		class Inner2{
			Inner2(){
				System.out.println("In Inner2");
			}
		
		}
	}

		public static void main(String[] args){
			
			Outer obj1=new Outer();
			Inner1 obj2=obj1.new Inner1();
		
			Inner1.Inner2 obj3=obj2.new Inner2();
		}
	
}
