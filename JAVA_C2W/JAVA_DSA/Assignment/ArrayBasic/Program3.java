
import java.util.*;
public class Program3 {
    public static void main(String[] args) {
        int[] arr = {3, 2, 1, 56, 10000, 167};

        // Using Arrays.sort() to sort the array in ascending order
        Arrays.sort(arr);

        // The first element will be the minimum, and the last element will be the maximum
        int min = arr[0];
        int max = arr[arr.length - 1];

        System.out.println("Minimum element: " + min);
        System.out.println("Maximum element: " + max);
    }
}
