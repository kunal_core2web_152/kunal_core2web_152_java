class replaceNum{
	public static void main(String[] args){

		int N=1004;
		int result=0;
		int place=0;

		for(int i=1; i<N; i++){
			int digit=N % 10;

			if(digit == 0){
				digit = 5;
			}

			result += digit * place;
			place *= 10;
			N /= 10;
		}
		System.out.println(result);
	}
}
